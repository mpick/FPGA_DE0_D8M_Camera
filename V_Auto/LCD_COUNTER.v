module LCD_COUNTER (
input CLK , // Video clock 25 MHz
input VS  , // Sync actif état bas
input HS  , // Sync actif état bas
input DE  , 
output reg [15:0] V_CNT , // compte lignes
output reg [15:0] H_CNT , // compte pixels
output reg  LINE ,
output reg  ACTIV_C,
output reg  ACTIV_V
) ; 
reg rHS ;
reg rVS ;

parameter H_OFF =12'd200 ; 
parameter V_OFF =12'd200 ; 

reg [15:0] H_CEN  ;//12'd450 ; // H_CNT max en fin de ligne -> 800 ?
reg [15:0] V_CEN  ;//12'd250 ; // V_CNT max en fin de ligne -> 525 ?


always @( posedge CLK  ) begin 
    ACTIV_V <=  HS & VS ; 
    rHS <= HS ;
	 rVS <= VS ;
	 //--H
	 if (!rHS &&  HS  ) begin  // Front montant de HS (fin de ligne complète 640+back porch+front porch+Hsync -> H_CNT=800)
	      { H_CNT ,H_CEN } <= {16'h0, H_CNT  }; 
	 end
	 //else if ( HS ) H_CNT <=H_CNT+1 ; 
	 else if ( DE ) H_CNT <=H_CNT+1 ;  // En cours de ligne active -> on compte les pixels
	 //--V
	 if (!rVS &&  VS  ) begin  // Front montant de VS (fin d'image complète avec top synchro)
	      { V_CNT , V_CEN} <= {16'h0 ,  V_CNT }; 
	 end
	 else if ((!rHS &&  HS  ) && (VS) )  V_CNT <=V_CNT+1 ; // Fin de ligne dans la zone verticale active -> on compte 1 ligne
	
	//--- H TRIGGER ---
	LINE  <= (
	 (( V_CNT == ( V_CEN/2 -V_OFF/2)  ) && ( H_CNT >= ( H_CEN/2 -H_OFF/2)  )  &&  ( H_CNT < ( H_CEN/2 +H_OFF/2)  )) ||
	 (( V_CNT == ( V_CEN/2 +V_OFF/2)  ) && ( H_CNT >= ( H_CEN/2 -H_OFF/2)  )  &&  ( H_CNT < ( H_CEN/2 +H_OFF/2)  )) ||
	 (( H_CNT == ( H_CEN/2 -H_OFF/2)  ) && ( V_CNT >= ( V_CEN/2 -V_OFF/2)  )  &&  ( V_CNT < ( V_CEN/2 +V_OFF/2)  )) ||
	 (( H_CNT == ( H_CEN/2 +H_OFF/2)  ) && ( V_CNT >= ( V_CEN/2 -V_OFF/2)  )  &&  ( V_CNT < ( V_CEN/2 +V_OFF/2)  ))
	 ) ?
	 1:0 ; 
	//--- V TRIGGER ---	
	ACTIV_C <= ( 
	  (( H_CNT >= ( H_CEN/2 -H_OFF/2)  ) &&  ( H_CNT < ( H_CEN/2 +H_OFF/2)  ))
	   &&
	  (( V_CNT >= ( V_CEN/2 -V_OFF/2)  ) &&  ( V_CNT < ( V_CEN/2 +V_OFF/2)  ))
	  )?1:0; 
end


endmodule 