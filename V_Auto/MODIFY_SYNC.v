module MODIFY_SYNC   (
input  PCLK  , 
input  S , // actif état bas ou état haut ?
output reg MS // actif état bas 
);

// Hypothèse : MS sera actif à l'état bas peu importe le mode de S ?

reg [15:0] NUM_H,NUM_L ; 

reg [15:0] CNT; 
reg rS  ; 


always @(posedge PCLK)begin 
  rS  <=  S  ; 
  MS  <=  ( NUM_H > NUM_L )? S :~S ; 
  //LEVEL LOW COUNTER: activation du sync: activation du sync
       if ( rS & !S  ) { NUM_H , CNT } <=  {CNT  , 16'h0 }   ; // Front descendant de S
  else if ( !rS & S  ) { NUM_L , CNT } <=  {CNT  , 16'h0 }   ; // Front montant de S
  else CNT<=CNT+1 ; 
  end
  
  
 endmodule 