################################################################################
#
# Filename: cycloneiii_m9k_check.tcl
#
#  Rev.B
#
# Description: This script detects if the design has any M9K memory block 
#   usage that may be susceptible to the Cyclone III read issue.
#
# Usage (GUI) :	 Open the project to be analyzed with the Quartus II.  From the 
#   Quartus II menu, go to Tools > Tcl Scripts. Select the script and click
#   Run to analyze your design.
#
# Usage (CUI) :	 quartus_cdb -t cycloneiii_m9k_check.tcl -project <project_name> -revision <revision_name>
#
# Input:	The project must be a successfully compiled design and the script
#   should be run on the Quartus II version with which the design was compiled.
#
# Output:
#  - Reporting the number of susceptible M9K blocks Part of Quartus output 
#     messages on System Messages Tab. 
#  - Two report files: 
#       <revision_name>.m9k.csv : Summary of each M9K block usage in 
#                                  Comma-Separated Variables format 
#       <revision_name>.m9k.rpt : Datailed instance report of each M9K.
#
# Author: 	Altera Corporation 
#		Copyright (c) Altera Corporation 2010.
#		All rights reserved.
#
################################################################################

#     Quartus II version is stored in $::quartus(version)
if {! [regexp {Version ([\d\.]+)} $::quartus(version) match version]} {
  post_message -type error "Unexpected Quartus II version: $::quartus(version)"
  return
} else {
  post_message -type info "Quartus II software version: $version"
}

proc puts_messages {res} {
  foreach line [split $res \n] {
    set type info
    if {! [regexp {^W*(Info|Warning|Critical Warning|Error): (.*)$} $line x type msg]} {
      continue
    }
    regsub " " $type _ type
    if { [info exists msg] } {
      post_message -type $type $msg
    }
  }
}

# For quartus_sh
if { $::quartus(nameofexecutable) ne "quartus_cdb" } {
  # Run quartus_cdb
  set cmd quartus_cdb
  if {[info exists quartus(binpath)]} {
    set cmd [file join $quartus(binpath) $cmd]
  }

  post_message -type info "Running quartus_cdb ..."
  if {[catch {exec $cmd -t "[info script]" -project $quartus(project) -revision $quartus(settings)} res]} {
    post_message -type error "Error in quartus_cdb"
    puts_messages $res
  } else {
    puts_messages $res
  }
  return
} else {
  load_package atoms
  package require cmdline

  # Open project
  set options {
    { "project.arg" "" "Project name" }
    { "revision.arg" "" "Revision name" }
  }
  set usage "Usage: quartus_cdb -t [info script] -project <project> -revision <revision>"
  array set opt [::cmdline::getoptions ::argv $options $usage]

  if { ($opt(project) eq "") || ($opt(revision) eq "") } {
    post_message -type error "$usage"
    # ToDo: Need to test
    return 0
  }

  if {[catch {project_open $opt(project) -revision $opt(revision)} res]} {
    post_message -type error "$res"
    post_message -type error "$usage"
    # ToDo: Need to test
    return 0
  }


  # Open file for output
  set rpt_filename ${opt(revision)}.m9k.rpt
  if {[catch {open "$rpt_filename" "w"} res]} {
    post_message -type error "$res"
    post_message -type error "Failed to open $rpt_filename"
    return 0
  } else {
    set fileId $res
  }


  # Open file for output in CSV
  set csv_filename ${opt(revision)}.m9k.csv
  if {[catch {open "$csv_filename" "w"} res]} {
    close $fileId
    post_message -type error "$res"
    post_message -type error "Failed to open $csv_filename"
    return 0
  } else {
    set csv_fileId $res
  }

  puts $fileId "Cyclone III M9K report for $::quartus(project)"
  puts $fileId [clock format [clock seconds]]
  puts $fileId "Quartus II $::quartus(version)"
  set dev [get_global_assignment -name device]
  puts $fileId "Device: $dev"
  puts $fileId ""
  puts $fileId ""

  puts $csv_fileId "Clock_Mode,M9K_ATOM_name,PortA_IN,PortA_OUT,PortB_IN,PortB_OUT,Location,CLK0_name,CLK0_inv,CLK1_name,CLK1_inv,Susceptible"

  read_atom_netlist

  set atom_col [get_atom_nodes -type "RAM"]
  set num_high_risk 0

  foreach_in_collection atom $atom_col {
      
    set atom_name [get_atom_node_info -key NAME -node $atom]
    set atom_location [get_atom_node_info -key LOCATION -node $atom]

    regexp {^M9K_X(\d+)_Y(\d+)_N.} $atom_location m xloc yloc
    set m9k_location [format "M9K_X%03d_Y%03d" $xloc $yloc]

    puts $fileId "M9K_ATOM $atom_name"

    puts $fileId "    LOC: $atom_location"
    # $atom_name is M9K instance name

    set clk0_name ""
    set clk0_invert_clock ""
    set clk1_name ""
    set clk1_invert_clock ""

    set portA_data_width 0
    set portB_data_width 0

    ###########################
    # Check input ports
    #
    foreach i [get_atom_iports -node $atom] {

      set iport_type [get_atom_port_info -node $atom -type iport -port_id $i -key type]
      set iport_bit_index [get_atom_port_info -node $atom -type iport -port_id $i -key literal_index]

      # $i is CLK port

      set fanin [get_atom_port_info -node $atom -type iport -port_id $i -key fanin]

      if {[llength $fanin] == 0} { 
        if { [regexp {CLK\d} $iport_type] } { 
          puts $fileId "    $iport_type: (not connected)"
        }
      } else {


    # $i CLK port is connected with $fanin signal 
        set fanin_node [get_atom_node_info -node [lindex $fanin 0] -key NAME]
        set invert_clock [get_atom_port_info -node $atom -type iport -port_id $i -key is_inverted]


        if  { "$fanin_node" != "" } {
          if  { "$iport_type" == "PORTADATAIN" } {
            puts $fileId "    $iport_type bit $iport_bit_index $fanin_node"

            incr portA_data_width
          }
          if  { "$iport_type" == "PORTBDATAIN" } {
            puts $fileId "    $iport_type bit $iport_bit_index $fanin_node"
            incr portB_data_width
          }
        }


        if { "$iport_type" == "CLK0" } {
          puts $fileId "    $iport_type: $fanin_node : INV $invert_clock"
          set clk0_name $fanin_node
          set clk0_invert_clock $invert_clock
        } 

        if { "$iport_type" == "CLK1" } {
          puts $fileId "    $iport_type: $fanin_node : INV $invert_clock"
          set clk1_name $fanin_node
          set clk1_invert_clock $invert_clock

        }
      }
      
    }
    #
    ###########################


    set portA_datao_width 0
    set portB_datao_width 0
    ###########################
    # Check output ports
    #
    foreach i [get_atom_oports -node $atom] {

      set oport_type [get_atom_port_info -node $atom -type oport -port_id $i -key type]
      set oport_bit_index [get_atom_port_info -node $atom -type oport -port_id $i -key literal_index]
      set oport_name [get_atom_port_info -node $atom -type oport -port_id $i -key name]

      set fanout [get_atom_port_info -node $atom -type oport -port_id $i -key fanout]
      
      if { [llength $fanout] != 0} { 

        if  { "$oport_type" == "PORTADATAOUT" } {
          puts $fileId "    $oport_type bit $oport_bit_index $oport_name"
          incr portA_datao_width
        }
        if  { "$oport_type" == "PORTBDATAOUT" } {
          puts $fileId "    $oport_type bit $oport_bit_index $oport_name"
          incr portB_datao_width
        }
      }
    }
    #
    ###########################

    ###########################
    # Check clock mode
    #
    set dual_clock 0
    set clock_mode "single"
    if { "$clk0_name" != "" && "$clk1_name" != "" } {
      if { ("$clk0_name" != "$clk1_name") || ($clk0_invert_clock != $clk1_invert_clock) } {
        puts $fileId "  ==== Dual Clock Mode ===="
        set dual_clock 1
        set clock_mode "dual"
      } else {
        puts $fileId "  ==== Single Clock Mode ===="
        set dual_clock 0
        set clock_mode "single"
      }
    }
    #
    ###########################


    puts $fileId "  Port A DATA(in,out): $portA_data_width $portA_datao_width"
    puts $fileId "  Port B DATA(in,out): $portB_data_width $portB_datao_width"


    ###########################
    # check if Susceptible
    set is_risk ""
    if { $portA_datao_width > $portB_datao_width } {
      set read_data_width $portA_datao_width
    } else {
      set read_data_width $portB_datao_width
    }
    if { $portA_data_width > $portB_data_width } {
      set write_data_width $portA_data_width
    } else {
      set write_data_width $portB_data_width
    }
    if { $dual_clock == 1 } {
      if { ($write_data_width > 18 ) && ($read_data_width > 2 ) } {
        set is_risk "Yes"
      }
      if { ($write_data_width > 0  ) && ($read_data_width > 18) } {
        set is_risk "Yes"
      }
    }
    #
    ###########################

    if { $is_risk ne "" } {
      incr num_high_risk 
      puts $fileId "  Susceptible"
    }
    puts $fileId " "
    puts $csv_fileId "$clock_mode,$atom_name,$portA_data_width,$portA_datao_width,$portB_data_width,$portB_datao_width,$m9k_location,$clk0_name,$clk0_invert_clock,$clk1_name,$clk1_invert_clock,$is_risk"

  } 
  # end foreach RAM atom


  close $fileId
  close $csv_fileId


  post_message -type info ""
  post_message -type info " *******************************************************************"
  post_message -type info ""
  if { $num_high_risk > 0 } {
    if { $num_high_risk > 1 } {
      post_message -type info "  Your design has $num_high_risk M9Ks in dual clock x32/x36 data width mode" 
    } else {
      post_message -type info "  Your design has $num_high_risk M9K in dual clock x32/x36 data width mode" 
    }
    post_message -type info "  that may be susceptible to the read bit error." 
    post_message -type info "" 
    post_message -type info "  Refer to $csv_filename file for the list of susceptible M9Ks." 
  } else {
    post_message -type info "  Your design has No M9Ks in dual clock x32/x36 data width mode."  
  }
  post_message -type info ""
  post_message -type info " *******************************************************************"
  post_message -type info ""
    
  return 1
}
