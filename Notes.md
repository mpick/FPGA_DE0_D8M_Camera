
## Éventuellement utile plus tard :
* La caméra peut embed des informations dans le signal vidéo (dans le header ou le footer), comme des infos sur l'exposition, la moyenne-min-max des signaux, le gain... -> Ca peut aider pour le seuillage propre ?
* Il serait intéressant de creuser le système QSys pour intégrer un processeur (*Nios*) dans le FPGA, ce qui permettrait l'exécution de code en C (comme ce qui est fait dans les exemples avancés de la D8M). 

## Bugs/Todo
[ ] L'autofocus n'est pas *smooth* comme il l'était avec le programme précédent : il fait le *sweep* lentement de manière saccadée puis saute à la valeur déterminée trop rapidement -> "tac" audible.
[v] Le VGA ne réagit pas correctement si j'envoie "1111" ou "0000" sur un canal, on perd alors toute l'image (image noire partout). 
    Hypothèse : c'est parce qu'on ne tient pas compte des périodes de blanking, HS,VS... -> on enverrait ces données aussi dans des temps 'interdits' ?
    Le module d'autofocus récupère ces infos -> analyser ce qu'il en fait. 
[ ] Le compteur de fps affiche parfois "00" alors que l'image est affichée.
[v] Ecrire un compteur de pixels en s'inspirant de LCD_COUNTER pour avoir résolution 32x24 par exemple (division par 20).
[ ] Avec plsuieurs frame buffers, faire une "traînée" pour mieux voir mouvements.
[ ] Prévoir une sortie binaire "mouvement détecté" ?
[v] Certaines lignes verticales immobiles sont détectées en "mouvement "en permanence, ça ne vient pas du bruit -> vérifier les indices de comparaison dans la mémoire ?
[v] Stocker en RAM à une fréquence moindre (toutes les 10 images par exemple) pour améliorer la détection et la rendre plus visible.
    Ca fonctionne bien, mais le rendu est un peu étrange : l'image actuelle bouge à 60Hz dans un "masque" bien plus lent. On comprend mal l'image.
[ ] Ralentir fréquence de rafraîchissement
    - Soit je n'affiche qu'une image tous les x temps, mais alors p-ê embêtant d'avoir des courts "flashes" tout le temps (on n'afficherait du noir la majorité du temps !) (EDIT: oui, ça tue les yeux)
    - Soit il faut un frame buffer (en couleurs) pour maintenir l'affichage rafraîchi à 60Hz sur une image fixe. Possible avec RAM externe, ou en basse résolution (compliqué ?) ou en seuillé (et encore, pas en haute résolution)


## Notes de compréhension
* Étonnant : le début de la trame active VGA était fixé à X_START = H_SYNC_CYC+ H_SYNC_BACK; (et idem pour le vertical), c'est à dire pendant le back porch puisque le compteur commence à la fin de la trame active/au début du front porch. J'ai modifié pour que X_START = H_SYNC_FRONT + H_SYNC_CYC + H_SYNC_BACK, on récupère quelques pixels visibles (sinon, en démarrant trop tôt on n'affichait pas les premiers pixels et la trame finissait tôt) et ça a surtout plus de sens. 
* Si je dois réduire la résolution (par moyennage de groupes de 20 pixels par exemple), j'introduis un retard d'au moins 2 (3) images : On ne peut plus comparer les pixels à la volée, il faut attendre 
    * Une première image à mettre en mémoire en basse résolution (20 pixels -> moyennes sur X -> 32x480 -> moyennes sur Y -> 32x24)
    * Une seconde image en mémoire pour comparer à la première -> stocker comparaison dans 3e buffer ? ou pas nécessaire car comparaison à la volée possible ?
    * La troisième image peut afficher les résultats de la comparaison des 2 premières.

    -> il faut donc 
    * 1 buffer 20x1 (pixels à moyenner)
    * 1 buffer 32x480
    * 2 buffers 32x24
* La compilation était beaucoup trop longue (bloque à 11% pendant 20 minutes) avec un frame buffer "maison" tout en blocs logiques (et pas dans un composant séparé). Passage facile à une RAM (Megafunction)