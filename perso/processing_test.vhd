library ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

ENTITY processing_test IS	 
PORT(
		clk25        : in  STD_LOGIC;							-- 25 MHz VGA clock
		iRed         : in  STD_LOGIC_VECTOR(7 downto 0);
		iGreen       : in  STD_LOGIC_VECTOR(7 downto 0);
		iBlue        : in  STD_LOGIC_VECTOR(7 downto 0);
		iVGA_BLANK_N : in  STD_LOGIC; -- blanking signal, 1 when VGA must output "active", visible data
		iVGA_HS      : in  STD_LOGIC; -- Horizontal sync signal (lines)
		iVGA_VS      : in  STD_LOGIC; -- Vertical sync signal (frames)

		sRED         : out STD_LOGIC_VECTOR(3 downto 0);
		sGREEN       : out STD_LOGIC_VECTOR(3 downto 0);
		sBLUE        : out STD_LOGIC_VECTOR(3 downto 0);

		SW           : in  STD_LOGIC_VECTOR(2 downto 0);
		SW_thr       : in  STD_LOGIC_VECTOR(1 downto 0);
		SW_delay     : in  STD_LOGIC_VECTOR(2 downto 0);

		oRAM_wAddr   : out STD_LOGIC_VECTOR(18 downto 0);
		oRAM_rAddr   : out STD_LOGIC_VECTOR(18 downto 0);
		oRAM_data    : out STD_LOGIC;
		oRAM_wren    : out STD_LOGIC;
		iRAM_q       : in  STD_LOGIC
	);
END processing_test;

ARCHITECTURE behav OF processing_test IS
	-- counters
	signal rVGA_HS, rVGA_VS : std_logic; -- buffer of VGA_HS to detect its edge
	signal rX_CNT, rY_CNT : integer range 0 to 307200 := 0; -- pixel counter and line counter -- range was 640, must be 640*480 to hold multiplication
	--signal rX_low_CNT, rY_low_CNT : integer range 0 to 32 := 0; -- low resolution (32x24) pixel and line counters
	--signal cFDelay : integer := 30;             -- number of frames between each RAM write
	signal rF_CNT  : integer range 0 to 1000 := 0; -- counts frames to write in RAM

	-- Grey levels conversion
	signal wY      : unsigned(15 downto 0); -- Luminance from iRed, iGreen, iBlue. Useful signal is from (15 downto 8)
	signal wThresh : std_logic; -- Thresholded VGA signal

	-- Pixels average (on 4 bits of luminance)
	--type tX_data is array(0 to 19) of unsigned(7 downto 4); -- keeps one group of pixel data to average (20 pixels)
	--type tWork_data is array(0 to 31, 0 to 479) of unsigned(7 downto 4); -- not sure if synthesizable -> may need to be flattened.
	--signal rXArray_data : tX_data := (others=>(others=>'0'));
	--signal rWorking_data : tWork_data := (others=>(others=>'0')); -- one horizontally-downscaled image

	-- Frame buffer
	--type tFrameBuff_full is array(0 to 639, 0 to 479) of std_logic; -- keeps one full resolution thresholded frame (x,y)
	--signal rFB :tFrameBuff_full := (others=>(others=>'0'));
	--type tFrameBuff_low is array(0 to 31, 0 to 23) of std_logic; 
	--signal rFB0 : tFrameBuff_low := (others=>(others=>'0')); -- first frame buffer, made by averaging input frame in both dimensions
	--signal rFB1 : tFrameBuff_low := (others=>(others=>'0')); -- second frame buffer


begin

-- Grey levels conversion
-- Y = R *//.299 = 256 * 0.299 = 77 // + G *// .587 = 256 * .587 = 150 //+ B *//.114 = 256 * .114 = 29
wY <= unsigned(iRed)*77 + unsigned(iGreen)*150 + unsigned(iBlue)*29 ; -- useful signal is now in wY(15 downto 8) 
wThresh <= '1' when ( wY(15 downto 8) > ( 40*( unsigned("000000" & SW_thr) + 1 ) ) ) else -- 40, 80, 160 or 200 (of 256)
		'0';

-- Active image pixel counter (pixels 0->639, lines 0->479)
--process(clk25)
--	begin
--	if rising_edge(clk25) then
--		-- count lines
--		rVGA_HS <= iVGA_HS; -- to detect rising edge
--		if( iVGA_VS='0' or rY_CNT >= 480) then -- end of image -> reset line counter
--			rY_CNT <= 0;
--		elsif( rVGA_HS='0' and iVGA_HS='1' ) then -- rising edge = end of pulse
--			rY_CNT <= rY_CNT + 1;
--		end if;

--		-- count pixels
--		if( iVGA_BLANK_N='1' ) then 

--			if( rX_CNT < 640 ) then 
--				rX_CNT <= rX_CNT + 1;
--			else rX_CNT <= 0;
--			end if;

--		else -- from if( iVGA_BLANK_N='1' )
--			rX_CNT <= 0;
--		end if;
--	end if;
--end process;

-- Processing
process(clk25)
	begin
	if rising_edge(clk25) then
---- Count frames, reset counters and control RAM write enable
		rVGA_VS <= iVGA_VS; -- to detect its edge
		if(iVGA_VS='0' and rVGA_VS='1') then -- falling edge of VSync -> end of image
			rX_CNT <= 0; -- reset counter if end of image
			--if(rF_CNT < cFDelay) then
			if(rF_CNT < 8*unsigned("0000"& SW_delay)) then
				rF_CNT <= rF_CNT + 1; -- count image
				oRAM_wren <= '0';
			else 
				rF_CNT <= 0;
				oRAM_wren <= '1'; -- write to RAM every <cFDelay> frames
			end if;
		end if;

		
		if(iVGA_BLANK_N='1') then -- active image

---- Count pixels (0 -> 307200-1)
		if( rX_CNT < 307200 ) then 
			rX_CNT <= rX_CNT + 1; -- count pixels
		else rX_CNT <= 0;
		end if;

---- Write the thresholded image in memory
		-- oRAM_addr <= std_logic_vector(to_unsigned(rX_CNT+(rY_CNT*640), oRAM_addr'length)); -- RAM configured to read old data when rewritten -> valid for all comparisons
		oRAM_wAddr <= std_logic_vector(to_unsigned(rX_CNT,oRAM_wAddr'length));
		oRAM_rAddr <= std_logic_vector(to_unsigned(rX_CNT+3,oRAM_rAddr'length)); -- rX_CNT+3 needed to get a perfect detection (no ghost vertical lines).
		--rFB(rX_CNT,rY_CNT) <= wThresh;
		oRAM_data <= wThresh;

---- Select output with the switches
		case SW is
		
		when "100" => -- movement detection overlay
			-- compare the present pixel with the last frame's pixel
			--if( wThresh /= rFB(rX_CNT, rY_CNT) ) then
			if( wThresh /= iRAM_q ) then
				if(unsigned('0'&iRed(7 downto 4)) +4 < "01111") then 
					sRED <= std_logic_vector( unsigned(iRed(7 downto 4)) +4 ); -- to see even the moving black pixels, we don't permit 0 values
					--sRED <= iRed(7 downto 4) + 2;
				else sRED <= "1111";
				end if;
				sGREEN <= "0000";
				sBLUE  <= "0000";
			else
				sRED   <= iRed(7 downto 4);
				sGREEN <= iGreen(7 downto 4);
				sBLUE  <= iBlue(7 downto 4);
			end if;

		when "101" => -- movement detection (color image)
			if( wThresh /= iRAM_q ) then
				sRED   <= iRed(7 downto 4);
				sGREEN <= iGreen(7 downto 4);
				sBLUE  <= iBlue(7 downto 4);
			else
				sRED   <= "0000";
				sGREEN <= "0000";
				sBLUE  <= "0000";
			end if;

		when "010" => -- Show grey levels (luminance only)
			sRED   <= std_logic_vector(wY(15 downto 12));
			sGREEN <= std_logic_vector(wY(15 downto 12));
			sBLUE  <= std_logic_vector(wY(15 downto 12));

		when "011" => -- movement detection (grey levels (luminance only))
			if( wThresh /= iRAM_q ) then
				sRED   <= std_logic_vector(wY(15 downto 12));
				sGREEN <= std_logic_vector(wY(15 downto 12));
				sBLUE  <= std_logic_vector(wY(15 downto 12));
			else
				sRED   <= "0000";
				sGREEN <= "0000";
				sBLUE  <= "0000";
			end if;

		when "110" => -- Show thresholding
			-- compare the present pixel with the last frame's pixel
			--if( wThresh /= rFB(rX_CNT, rY_CNT) ) then
			if( wThresh /= iRAM_q ) then
				sRED   <= "1111";
				sGREEN <= "0000";
				sBLUE  <= "0000";
			else
				sRED   <= (others => wThresh);
				sGREEN <= (others => wThresh);
				sBLUE  <= (others => wThresh);	
			end if;

		when "111" => -- Show thresholding, movement only
			-- compare the present pixel with the last frame's pixel
			--if( wThresh /= rFB(rX_CNT, rY_CNT) ) then
			if( wThresh /= iRAM_q ) then
				sRED   <= "1111";
				sGREEN <= "1111";
				sBLUE  <= "1111";
			else
				sRED   <= (others => '0');
				sGREEN <= (others => '0');
				sBLUE  <= (others => '0');	
			end if;

		-- Default case : show input
		when others =>
			sRED   <= iRed(7 downto 4);
			sGREEN <= iGreen(7 downto 4);
			sBLUE  <= iBlue(7 downto 4);
		end case;
			
		else -- from if(iVGA_BLANK_N='1')
		sRED   <= "0000";
		sGREEN <= "0000";
		sBLUE  <= "0000";
		end if;
	end if;
	end process;
end behav;	
